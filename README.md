山本さん作のMCC製作実績一覧サイトをmcc-ds.comに移植したものです。  
一覧は  
[http://mcc-ds.com/tools/worklist/](http://mcc-ds.com/tools/worklist/)  
管理画面は  
[http://mcc-ds.com/tools/worklist/admin/login.php](http://mcc-ds.com/tools/worklist/admin/login.php)  

basic認証 (medico / site)  
管理画面 (yamamoto / taka4) 、(medico / site)   
1アカウントで共有してもらっても問題無さそうな気がします。  

まだ使い方もよくわかっていませんが、**新人**ディレクターのためのツールということです。  
今後、ディレクターの意見を聞いて使いやすくしてください、とのことです。  

## データベースについて  
【DS1 Plesk】のデータベース名＝[worklist]　がそれです。  

## FTPについて
mcc-ds.comにFTP接続して、  
WEBサイト　⇒　/httpdocs/tools/worklist  
管理画面　⇒　/httpdocs/tools/worklist/admin  
★toolsをわざわざ作ったのは、便利ツールが今後増えた時の置き場所とするためです。  


#### 栗原より  
* 移植では、当社サーバに合わせて「リダイレクト」「パス指定」の修正に苦労しました。  
一部、URLパラメータを使っているページでは、仕方なくbaseタグを使ってパスを合わせています。  
これでバグが無いかどうか、まだまだ検証が必要です。  
元々のソースはこちらにあります。  
\\\\FILESERVER2\interoffice\システム関連\製作実績サイトの原本\MCC制作サイト集  


#### 読み込んでいるJavaScriptについて
* vague.js　・・・　画面をぼやけさせる  
* jquery.autopager-1.0.0.min.js　・・・　オートページャー  
* piclist.js　・・・　オートページャーの設定  