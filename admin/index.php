<?php
require_once("./common/config.inc");
$db = DB_connect();

if (isset($_FILES['upfile'])) {
	$upfile = $_FILES['upfile']['name'];
	$upfile_path = $_FILES['upfile']['tmp_name'];
	if ($upfile == "worklist.xlsx" || $upfile == "worklist.xls" || $upfile == "worklist.csv") {
		require_once("./ul_data.php");
		header("location: ./index.php");
		exit();
	}
}

$filter_ary['key'] = $filter_ary['name'] = $filter_ary['type'] = $filter_ary['kamoku'] = $filter_ary['image'] = $filter_ary['layout'] = $filter_ary['color'] = $filter_ary['open'] = "";
if (isset($_GET['filter']) && $_GET['filter'] == "on") {
	if (isset($_GET['name'])) {
		$where_str = " where `site_name` like '%{$_GET['name']}%' or `site_url` like '%{$_GET['name']}%' ";
		$filter_ary['name'] = $_GET['name'];

	} elseif (isset($_GET['key'])) {
		$where_str = " where `keyword` like '%{$_GET['key']}%' ";
		$filter_ary['key'] = $_GET['key'];

	} else {
		$where_str = " where 1 ";
		if (isset($_GET['type'])) {
			$where_str .= " and `type_cd`='{$_GET['type']}' ";
			$filter_ary['type'] = $_GET['type'];
		}
		if (isset($_GET['kamoku'])) {
			$where_str .= " and (`kamoku_cd` like '{$_GET['kamoku']},%' or `kamoku_cd` like '%,{$_GET['kamoku']},%' or `kamoku_cd` like '%,{$_GET['kamoku']}') ";
			$filter_ary['kamoku'] = $_GET['kamoku'];
		}
		if (isset($_GET['image'])) {
			$where_str .= " and `image_cd`='{$_GET['image']}' ";
			$filter_ary['image'] = $_GET['image'];
		}
		if (isset($_GET['layout'])) {
			$where_str .= " and `layout_cd`='{$_GET['layout']}' ";
			$filter_ary['layout'] = $_GET['layout'];
		}
		if (isset($_GET['color'])) {
			$where_str .= " and `color_cd`='{$_GET['color']}' ";
			$filter_ary['color'] = $_GET['color'];
		}
		if (isset($_GET['open'])) {
			$where_str .= " and `open_flag`='{$_GET['open']}' ";
			$filter_ary['open'] = $_GET['open'];
		}
	}

} elseif (isset($_GET['filter'])) {
	header("Location: ./index.php");
	exit();
}

$cnt_sql = "SELECT count(id) FROM site ".$where_str;
$cnt_res = mysql_query($cnt_sql);
$cnt_row = mysql_fetch_row($cnt_res);
$total = $cnt_row[0];

$page = $_REQUEST['page'];
if(empty($page))	{
	$page = 1;
}
if( $page == 1 )	{
	$start = 0;
	$limit = 50;
}else{
	$start = ($page-1)*50;
	$limit = 50*$page;
}
//ID順にデータを取得
$sql = "SELECT * FROM site {$where_str} ORDER BY id DESC LIMIT ".$start.",".$limit;
$res = mysql_query($sql);
$i=0;
while( $row = mysql_fetch_assoc($res))	{
	$sites[$i]	= $row;
	$i++;
}
mysql_close($db);
require_once("./include/header.inc");
?>
<div id="main">
	<h2>登録サイト管理</h2>
	<div class="contents">
		<div class="c_header">
			<h3>登録サイト一覧　
				<span style="font-size:12pt;margin-left:100px;">
					(<label><input type="radio" name="dl_type" value="xlsx">xlsx</label>
					<label><input type="radio" name="dl_type" value="xls">xls</label>
					<label><input type="radio" name="dl_type" value="csv" checked>csv</label>)で
				</span>
				<input id="dl_data_btn" style="font-size:10pt;padding:2px;" type="button" value="一覧をダウンロード">
				<form id="ul_form" style="float:right;" action="./index.php" enctype="multipart/form-data" method="post">
					<span style="margin-left:100px;font-size:10pt;">一覧ファイル：</span><input id="ul_file_name" type="file" name="upfile"><span style="font-size:12pt;">を</span>
					<input id="ul_data_btn" style="font-size:10pt;padding:2px;" type="button" value="上書きアップロード">
				</form>
				<br style='clear:both;'>
			</h3>
		</div>
		<div>
			<a style="float:left;" href="new.php">新規登録</a>
			<a style="float:right;" href="./index.php">フィルタ解除</a>
			<br style='clear:both;'>
		</div>
		<section id="sec02">
			<table class="demo02">
			    <tr>
			        <th>サイト名<br><input id="srch_name" type="text" value="<?=$filter_ary['name'] ?>"></th>
					<th>キーワード<br><input id="srch_key" type="text" style="width:100px;" value="<?=$filter_ary['key'] ?>"></th>
					<th>タイプ<br>
						<select id="filter_type">
							<option value="">フィルタ</option>
							<?php foreach($type_array as $no =>$ary) { ?>
								<option value="<?=$ary['code'] ?>" <?=($filter_ary['type']==$ary['code'])?"selected":"" ?>><?=$ary['name'] ?></option>
							<?php } ?>
						</select>
					</th>
			        <th width="30%">診療科目<br>
			        	<select id="filter_kamoku">
							<option value="">フィルタ</option>
				        	<?php foreach($kamoku_array as $no =>$ary) { ?>
				        		<option value="<?=$ary['code'] ?>" <?=($filter_ary['kamoku']==$ary['code'])?"selected":"" ?>><?=$ary['name'] ?></option>
				        	<?php } ?>
				        </select>
				    </th>
			        <th>イメージ<br>
			        	<select id="filter_image">
							<option value="">フィルタ</option>
				        	<?php foreach($image_array as $no =>$ary) { ?>
				        		<option value="<?=$ary['code'] ?>" <?=($filter_ary['image']==$ary['code'])?"selected":"" ?>><?=$ary['name'] ?></option>
				        	<?php } ?>
				        </select>
			        </th>
			        <th>レイアウト<br>
			        	<select id="filter_layout">
							<option value="">フィルタ</option>
				        	<?php foreach($layout_array as $no =>$ary) { ?>
				        		<option value="<?=$ary['code'] ?>" <?=($filter_ary['layout']==$ary['code'])?"selected":"" ?>><?=$ary['name'] ?></option>
				        	<?php } ?>
				        </select>
			        </th>
			        <th>カラー<br>
			        	<select id="filter_color">
							<option value="">フィルタ</option>
				        	<?php foreach($color_array as $no =>$ary) { ?>
				        		<option value="<?=$ary['code'] ?>" <?=($filter_ary['color']==$ary['code'])?"selected":"" ?>><?=$ary['name'] ?></option>
				        	<?php } ?>
				        </select>
			        </th>
					<th>状態<br>
			        	<select id="filter_open">
							<option value="">フィルタ</option>
			        		<option value="1" <?=($filter_ary['open']=="1")?"selected":"" ?>>公開</option>
			        		<option value="0" <?=($filter_ary['open']=="0")?"selected":"" ?>>非公開</option>
				        </select>
					</th>
			    </tr>
			<?php
			if (isset($sites)) {
				foreach( $sites as $s )	{
					$kamoku_cd = explode(",",$s['kamoku_cd']);
					?>
					<tr class="data_row <?php echo ($s['open_flag'] == 1) ? '' : 'bg_gray'; ?>">
				        <td><input class="site_id" type="hidden" value="<?=$s['id'] ?>"><a class="site_link" href="<?php echo $s['site_url']; ?>" target="_blank"><?php echo $s['site_name']; ?></a></td>
				        <td><?php echo $s['keyword']; ?></td>
						<td><?php echo $type_name[$s['type_cd']]; ?></td>
				        <td>
					<?php
					foreach( $kamoku_cd as $k )	{
						echo "<span>".$kamoku_name[$k]."</span>";
					}
					?>
						</td>
				        <td><?php echo $image_name[$s['image_cd']]; ?></td>
				        <td><?php echo $layout_name[$s['layout_cd']]; ?></td>
				        <td><?php echo $color_name[$s['color_cd']]; ?></td>
						<td><?=($s['open_flag']==1)?"公開":"非公開" ?></td>
					</tr>
					<?php
				}

			} else { ?>
				<tr><td style="text-align:center;" colspan="7">該当するサイトが見つかりません</td></tr>

			<?php } ?>

			</table>
		</section>
		<?php echo pager($page,$total); ?>
	</div>
</div>
<script>
$(function(){
	$("#dl_data_btn").on("click" , function(){
		location = "./dl_data.php?type=" + $("input[name=dl_type]:checked").val();
	});
	$("#ul_data_btn").on("click" , function(){
		var filepath = $("#ul_file_name").val();
		if (filepath.indexOf("worklist.xls") > -1 || filepath.indexOf("worklist.xlsx") > -1 || filepath.indexOf("worklist.csv") > -1) {
			if (window.confirm("上書きでアップロードします。よろしいですか？")) {
				$("#ul_form").submit();
			} else {
				return false;
			}

		} else {
			alert("データファイルを選択してください");
		}

	});
	$(".data_row").on("click" , function(){
		var site_id = $(".site_id" , this).val();
		location = "./detail.php?id=" + site_id;
	});
	$(".site_link").on("click" , function(e){
		e.stopPropagation();
	});

	$("#srch_name").keypress(function(eo){//文字入力キーが押された
		if (eo.which == 13) {//Enterキー
			location = "./index.php?filter=on&name=" + $(this).val();
		}
	});
	$("#srch_key").keypress(function(eo){//文字入力キーが押された
		if (eo.which == 13) {//Enterキー
			location = "./index.php?filter=on&key=" + $(this).val();
		}
	});
	$("#filter_type").on("change" , function(){
		$("#f_type").val( $(this).val() );
		do_filter();
	});
	$("#filter_kamoku").on("change" , function(){
		$("#f_kamoku").val( $(this).val() );
		do_filter();
	});
	$("#filter_image").on("change" , function(){
		$("#f_image").val( $(this).val() );
		do_filter();
	});
	$("#filter_layout").on("change" , function(){
		$("#f_layout").val( $(this).val() );
		do_filter();
	});
	$("#filter_color").on("change" , function(){
		$("#f_color").val( $(this).val() );
		do_filter();
	});
	$("#filter_open").on("change" , function(){
		$("#f_open").val( $(this).val() );
		do_filter();
	});
	function do_filter() {
		var cnt = 6;
		if ($("#f_type").val() == "") {
			$("#f_type").remove();
			cnt--;
		}
		if ($("#f_kamoku").val() == "") {
			$("#f_kamoku").remove();
			cnt--;
		}
		if ($("#f_image").val() == "") {
			$("#f_image").remove();
			cnt--;
		}
		if ($("#f_layout").val() == "") {
			$("#f_layout").remove();
			cnt--;
		}
		if ($("#f_color").val() == "") {
			$("#f_color").remove();
			cnt--;
		}
		if ($("#f_open").val() == "") {
			$("#f_open").remove();
			cnt--;
		}
		if (cnt > 0) {
			$("#filter_form").submit();
		} else {
			location = "./index.php";
		}
	}
});
</script>
<style>
.data_row:hover {
	background-color:#c5f9dd;
	cursor:pointer;
}
</style>
<form id="filter_form" method="get" action="./index.php">
	<input type="hidden" name="filter" value="on">
	<input id="f_type" type="hidden" name="type" value="<?=isset($_GET['type'])?$_GET['type']:"" ?>">
	<input id="f_kamoku" type="hidden" name="kamoku" value="<?=isset($_GET['kamoku'])?$_GET['kamoku']:"" ?>">
	<input id="f_image" type="hidden" name="image" value="<?=isset($_GET['image'])?$_GET['image']:"" ?>">
	<input id="f_layout" type="hidden" name="layout" value="<?=isset($_GET['layout'])?$_GET['layout']:"" ?>">
	<input id="f_color" type="hidden" name="color" value="<?=isset($_GET['color'])?$_GET['color']:"" ?>">
	<input id="f_open" type="hidden" name="open" value="<?=isset($_GET['open'])?$_GET['open']:"" ?>">
</form>
<?php
require_once("./include/footer.inc");
?>