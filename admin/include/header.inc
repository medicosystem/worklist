<!DOCTYPE html>
<html lang="ja">
<head>
<meta charset="UTF-8">
<title><?php echo $pageTitle; ?></title>
<meta name="robots" content="noindex,nofellow" />
<link rel="stylesheet" href="common/css/layout.css">
<!--<script src="common/js/common.js" charset="utf-8"></script>-->
<script type="text/javascript" src="common/js/jquery.min.js"></script>
<!--[if lt IE 9]><script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
</head>
<body>

<header id="header">
<h1><?php echo $pageTitle; ?></h1>
</header>
<?php
/*
<nav>
<ul class="global-nav">
	<li><a href="#">サイト管理</a></li>
	<li><a href="#">カテゴリー管理</a></li>
	<li><a href="#">サイト管理</a></li>
	<li><a href="#">カテゴリー管理</a></li>
	<li><a href="#">サイト管理</a></li>
	<li><a href="#">カテゴリー管理</a></li>
</ul>
</nav>
*/
?>