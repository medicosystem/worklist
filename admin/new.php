<?php
require_once("./common/config.inc");

$mode = $_REQUEST['mode'];

$db = DB_connect();

$error_flag	= 0;
if( $mode == 1 )	{
	$data	= $_REQUEST['data'];
	if( strlen(trim($data['site_name'])) == 0 )	{
		$error['site_name']	= "サイト名が未入力です";
		$error_flag++;
	}
	if (preg_match('/^(https?|ftp)(:\/\/[-_.!~*\'()a-zA-Z0-9;\/?:\@&=+\$,%#]+)$/', $data['site_url'])) {
	}else{
		$error['site_url']	= "URLが正しくありません";
		$error_flag++;
	}
/*
	if(isset($data['sort']))	{
		if( !is_numeric($data['sort'])) {
			$error['sort']	= "ソートは数値で入力してください";
			$error_flag++;
		}
	}
*/
	if( $error_flag < 1 )	{
		$now = date('Y-m-d H:i:s');
		//データ登録
		$sql = "INSERT INTO site SET ";
		$sql .= "site_name='".$data['site_name']."',";
		$sql .= "site_url='".$data['site_url']."',";
		if( isset($data["keyword"]))	{
			$data["keyword"] = array_filter($data["keyword"], 'strlen');
			$keyword = implode(',',$data["keyword"]);
			$sql .= "keyword='".$keyword."',";
		}
		if( isset($data["type_cd"]))	{
			$sql .= "type_cd=".$data['type_cd'].",";
		}
		if( isset($data["kamoku_cd"]))	{
			$data["kamoku_cd"] = array_filter($data["kamoku_cd"], 'strlen');
			$kamoku_cd = implode(',',$data["kamoku_cd"]);
			$sql .= "kamoku_cd='".$kamoku_cd."',";
		}
		if( isset($data["image_cd"]))	{
			$sql .= "image_cd=".$data['image_cd'].",";
		}
		if( isset($data["layout_cd"]))	{
			$sql .= "layout_cd=".$data['layout_cd'].",";
		}
		if( isset($data["color_cd"]))	{
			$sql .= "color_cd='".$data['color_cd']."',";
		}
		if( isset($data["memo"]))	{
			$sql .= "memo='".$data['memo']."',";
		}
		$sql .= "open_flag=".$data['open_flag'].",";
		if( isset($data["sort"]))	{
			$sql .= "sort=".$data["sort"].",";
		}
		$sql .= "c_user_id=".$admin['id'].",";
		$sql .= "m_user_id=".$admin['id'].",";
		$sql .= "created='".$now."',modified='".$now."';";
//		var_dump($sql);
//		exit();
		mysql_query($sql);

		$site_id = mysql_insert_id();
		/*
		画像UP処理
		************************************************************/
		//	var_dump($_FILES["upload_file"]);
			if ($_FILES["upload_file"]["tmp_name"] != "") { //画像送信しようとした場合
				if($_FILES["upload_file"]["type"] != "image/pjpeg" && $_FILES["upload_file"]["type"] != "image/jpeg")	{
				//JPEGファイルではない
					$error['img']	= "画像がJPEG形式ではありません";
					$error_flag++;
				} elseif($_FILES["upload_file"]["size"] > 2048*1024)	{
				//ファイルサイズが２MB以上
					$error['img']	= "ファイルサイズが多きすぎます２MB以下にしてください";
					$error_flag++;
				} elseif(!is_uploaded_file($_FILES["upload_file"]["tmp_name"]))	{
				//フォームからの送信でない
					$error['img']	= "画像がUPできませんでした";
					$error_flag++;
				} else {
					$tmp	= "../tmp/".$_FILES["upload_file"]["name"];
					$img	= "../images/site/".$site_id.".jpg";
					if(file_exists($img)) {
					//もしファイルが存在していたら削除
						unlink($img);
					}
					//アップロード
					move_uploaded_file($_FILES["upload_file"]["tmp_name"], $tmp);
					//画像サイズ取得
					$Jsize=getimagesize($tmp);
					if($Jsize[0] >= 201)	{
					//横121ピクセル以上なら
						$Jwidth		= 570; //横570ピクセル
						$Jheight	= $Jsize[1] * 570 / $Jsize[0]; //縦サイズを計算
						$imagein	= imagecreatefromjpeg($tmp); //画像を縮小する
						$imageout	= imagecreatetruecolor($Jwidth,$Jheight); //サイズ変更（GD2使用）
						imagecopyresampled($imageout,$imagein,0,0,0,0,$Jwidth,$Jheight,$Jsize[0],$Jsize[1]);
						imagejpeg($imageout,($img)); //サムネイル書き出し
						imagedestroy($imagein); //メモリを解放する
						imagedestroy($imageout); //メモリを解放する
					} else {
					//画像の横幅がもともと120ピクセル以下の場合は [thumb] ディレクトリにコピーします。
						copy($tmp,$img); //ファイルコピー
					}
					unlink($tmp); //[image] ディレクトリの画像を削除
					$img_name = $site_id.".jpg";
					$imgupd_sql = "UPDATE site SET img='".$img_name."' WHERE id=".$site_id;
					mysql_query($imgupd_sql);
				}
			}
		/*
		画像UP処理
		************************************************************/

		header("Location: ./index.php?mode=completion");
		exit();
	}else{
		$status = "<p class=\"err\">登録できませんでした</p>";
	}
}
//公開日時分割
if( isset($data['open_date']))	{
	$open_date = date("YmdHis",strtotime($data['open_date']));
	$o_d['Y'] = substr($open_date, 0, 4);
	$o_d['m'] = substr($open_date, 4, 2);
	$o_d['d'] = substr($open_date, 6, 2);
	$o_d['H'] = substr($open_date, 8, 2);
	$o_d['i'] = substr($open_date, 10, 2);
	$o_d['s'] = substr($open_date, 12, 2);
}

//メタキーワード分割
if( empty($data['keyword']))	{
	$data['keyword'] = ",,";
}else{
	$data["keyword"] = array_filter($data["keyword"], 'strlen');
	$data["keyword"] = implode(',',$data["keyword"]);
}
$keywords = explode(",",$data['keyword']);
//タグ分割
if( empty($data['kamoku_cd']))	{
	$data['kamoku_cd'] = ",,";
}else{
	$data["kamoku_cd"] = array_filter($data["kamoku_cd"], 'strlen');
	$data["kamoku_cd"] = implode(',',$data["kamoku_cd"]);
}
$kamoku_data = explode(",",$data['kamoku_cd']);
mysql_close($db);
?>
<?php
require_once("./include/header.inc");
?>
<div id="main">
<div class="contents">
<a href="./index.php">HOME</a>&nbsp;&raquo;&nbsp;新規登録
<form id="login-form" name="login-form" method="post" action="" enctype="multipart/form-data">
<input type="hidden" name="mode" value="1" />
<?php echo (isset($status)) ? $status : ''; ?>
<table class="cp-blue border shadow rd">
<tr>
	<th width="180">公開フラグ</th>
	<td><select name="data[open_flag]">
		<option value="0"<?php echo ($data['open_flag'] == "0") ? ' selected' : ''; ?>>非公開</option>
		<option value="1"<?php echo ($data['open_flag'] == "1") ? ' selected' : ''; ?>>公開</option>
	</select></td>
</tr>
<tr>
	<th width="180">サイト名（クリニック名）</th>
	<td><input name="data[site_name]" type="text" size="105" value="<?php echo $data['site_name']; ?>" required>
	<?php echo (isset($error['site_name'])) ? '<br /><span class="red">'.$error['site_name'].'</span>' : ''; ?></td>
</tr>
<tr>
	<th width="180">URL</th>
	<td><input name="data[site_url]" type="text" size="105" value="<?php echo $data['site_url']; ?>" required>
	<?php echo (isset($error['site_url'])) ? '<br /><span class="red">'.$error['site_url'].'</span>' : ''; ?></td>
</tr>
<tr>
	<th>キーワード</th>
	<td><?php
for( $i=0; $i<count($keywords); $i++ )	{
?>
	<input name="data[keyword][<?php echo $i; ?>]" type="text" class="keyword" size="20" value="<?php echo $keywords[$i]; ?>"> ,
<?php
}
?>
	<input name="data[keyword][<?php echo $i; ?>]" type="text" size="20" value="">
	<?php echo (isset($error['keyword'])) ? '<br /><span class="red">'.$error['keyword'].'</span>' : ''; ?></td>
</tr>
<tr>
	<th width="180">タイプ</th>
	<td><?php
for( $i=0; $i<count($type_array); $i++ )	{
?>	
	<label class="clear"><input type="radio" name="data[type_cd]" value="<?php echo $type_array[$i]['code']; ?>" <?=($i==count($type_array)-1)?"checked":"" ?>> <?php echo $type_array[$i]['name']; ?></label>
<?php
}
?></td>
</tr>
<tr>
	<th>診療科目</th>
	<td><?php
for( $i=0; $i<count($kamoku_array); $i++ )	{
?>
	<label class="clear"><input type="checkbox" name="data[kamoku_cd][<?php echo $i; ?>]" value="<?php echo $kamoku_array[$i]['code']; ?>" <?=($i==count($kamoku_array)-1)?"checked":"" ?>> <?php echo $kamoku_array[$i]['name']; ?></label>
<?php
}
?>
	<?php echo (isset($error['kamoku_cd'])) ? '<br /><span class="red">'.$error['kamoku_cd'].'</span>' : ''; ?></td>
</tr>
<tr>
	<th width="180">イメージ</th>
	<td><?php
for( $i=0; $i<count($image_array); $i++ )	{
?>	
	<label class="clear"><input type="radio" name="data[image_cd]" value="<?php echo $image_array[$i]['code']; ?>" <?=($i==0)?"checked":"" ?>> <?php echo $image_array[$i]['name']; ?></label>
<?php
}
?></td>
</tr>
<tr>
	<th width="180">レイアウト</th>
	<td><?php
for( $i=0; $i<count($layout_array); $i++ )	{
?>	
	<label class="clear"><input type="radio" name="data[layout_cd]" value="<?php echo $layout_array[$i]['code']; ?>" <?=($i==count($layout_array)-1)?"checked":"" ?>> <?php echo $layout_array[$i]['name']; ?></label>
<?php
}
?></td>
</tr>
<tr>
	<th width="180">カラー</th>
	<td><?php
for( $i=0; $i<count($color_array); $i++ )	{
?>	
	<label class="clear"><input type="radio" name="data[color_cd]" value="<?php echo $color_array[$i]['code']; ?>" <?=($i==0)?"checked":"" ?>> <?php echo $color_array[$i]['name']; ?></label>
<?php
}
?></td>
</tr>
<tr>
	<th>画像</th>
	<td><input name="upload_file" type="file" class="file">
	<?php echo (isset($error['img'])) ? '<br /><span class="red">'.$error['img'].'</span>' : ''; ?></td>
</tr>
</table>

<p class="btn"><input type="submit" value="追加する" id="submit" /></p>
</form>
</div>
<?php
require_once("./include/footer.inc");
?>