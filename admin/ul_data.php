<?php
require_once("./common/config.inc");
$db = DB_connect();

function _getText($objCell = null) {
	if (is_null($objCell)) {
		return false;
	}
	$txtCell = "";
	//まずはgetValue()を実行
	$valueCell = $objCell->getValue();
	if (is_object($valueCell)) {
		//オブジェクトが返ってきたら、リッチテキスト要素を取得
		$rtfCell = $valueCell->getRichTextElements();
		//配列で返ってくるので、そこからさらに文字列を抽出
		$txtParts = array();
		foreach ($rtfCell as $v) {
			$txtParts[] = $v->getText();
		}
		//連結する
		$txtCell = implode("", $txtParts);

	} else {
		if (!empty($valueCell)) {
			$txtCell = $valueCell;
		}
	}

	return $txtCell;
}

if ($upfile == "worklist.xlsx" || $upfile == "worklist.xls") {
	require_once("./include/Classes/PHPExcel.php");
	require_once './include/Classes/PHPExcel/IOFactory.php';

	//Excel読み込み
	if ($upfile == "worklist.xlsx") {
		$objReader = PHPExcel_IOFactory::createReader('Excel2007');
	} elseif ($upfile == "worklist.xls") {
		$objReader = PHPExcel_IOFactory::createReader('Excel5');
	}
	$book = $objReader->load($upfile_path);
	//シート設定
	$book->setActiveSheetIndex(0);
	$sheet = $book->getActiveSheet();
	// セルから値を取得
	$row = 2;
	while (1) {//row
		$objCell = $sheet->getCellByColumnAndRow(1, $row);//中身セル。col[0-],row[1-]の並び
		$table_val = _getText($objCell);//中身
		if ($table_val == "") break;//中身が空＝データ無し

		//セル配列作成
		$col = 1;//IDセルから
		while (1) {//col
			$objCell = $sheet->getCellByColumnAndRow($col - 1 , $row); //col,rowの並び
			$table_val = _getText($objCell); 
			$table_ary[$row - 2][$col - 1] = $table_val;

			$objCell = $sheet->getCellByColumnAndRow($col , 1); //col,rowの並び
			$table_val = _getText($objCell); 
			if ($table_val == "") break;//最終列

			$col++;
		}
		$row++;
	}
	$SQL = "delete from `site` ";
	$result = mysql_query($SQL);
	foreach ($table_ary as $no => $row_ary) {
		$SQL = "insert into `site`(`id`,
									`site_name`,
									`site_url`,
									`keyword`,
									`type_cd`,
									`kamoku_cd`,
									`image_cd`,
									`layout_cd`,
									`color_cd`,
									`open_flag`,
									`sort`
									) 
									values('{$row_ary[0]}',
											'{$row_ary[1]}',
											'{$row_ary[2]}',
											'{$row_ary[3]}',
											'{$row_ary[4]}',
											'{$row_ary[5]}',
											'{$row_ary[6]}',
											'{$row_ary[7]}',
											'{$row_ary[8]}',
											'{$row_ary[9]}',
											'{$row_ary[10]}'
									) ";
		$result = mysql_query($SQL);
	}

} elseif ($upfile == "worklist.csv") {
	$stream = fopen($upfile_path, 'r');
//	mb_convert_variables('SJIS' , 'UTF8' , $csv_header_ary);
}
