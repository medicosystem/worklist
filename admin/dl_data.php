<?php
require_once("./common/config.inc");
$db = DB_connect();

//ヘッダ部分
if ($_GET['type'] == "xlsx" || $_GET['type'] == "xls") {
	require_once("./include/Classes/PHPExcel.php");
	$book = new PHPExcel();
	$book->getProperties()->setCreator("Wings")->setTitle("サンプル");
	$col = 0;
	$book->setActiveSheetIndex(0)
		->setCellValueByColumnAndRow($col++ , 1 , "id")
		->setCellValueByColumnAndRow($col++ , 1 , "サイト名")
		->setCellValueByColumnAndRow($col++ , 1 , "サイトURL")
		->setCellValueByColumnAndRow($col++ , 1 , "キーワード")
		->setCellValueByColumnAndRow($col++ , 1 , "タイプID")
		->setCellValueByColumnAndRow($col++ , 1 , "診療科目ID")
		->setCellValueByColumnAndRow($col++ , 1 , "イメージID")
		->setCellValueByColumnAndRow($col++ , 1 , "レイアウトID")
		->setCellValueByColumnAndRow($col++ , 1 , "カラーID")
		->setCellValueByColumnAndRow($col++ , 1 , "状態")
		->setCellValueByColumnAndRow($col++ , 1 , "ソート番号");
	$sheet = $book->getActiveSheet();
	$sheet->getDefaultStyle()->getFont()->setName( 'ＭＳ ゴシック' )->setSize(9);

} else {
	$stream = fopen('./worklist.csv', 'w');
	$csv_header_ary = array("id" , 
							"サイト名",
							"サイトURL",
							"キーワード",
							"タイプID",
							"診療科目ID",
							"イメージID",
							"レイアウトID",
							"カラーID",
							"状態",
							"ソート番号",
							);
	mb_convert_variables('SJIS' , 'UTF8' , $csv_header_ary);
	fputcsv($stream, $csv_header_ary);
}

//データ部分
$result = mysql_query("set names utf8");
$SQL = "select * from `site` ";
$result = mysql_query($SQL);
$row = 2;
while ($rs_ary = mysql_fetch_assoc($result)) {
	$col = 0;
	if ($_GET['type'] == "xlsx" || $_GET['type'] == "xls") {
		$book->setActiveSheetIndex(0)
			->setCellValueByColumnAndRow($col++ , $row , $rs_ary['id'])
			->setCellValueByColumnAndRow($col++ , $row , $rs_ary['site_name'])
			->setCellValueByColumnAndRow($col++ , $row , $rs_ary['site_url'])
			->setCellValueByColumnAndRow($col++ , $row , $rs_ary['keyword']!=""?$rs_ary['keyword']:" ")
			->setCellValueExplicitByColumnAndRow($col++ , $row , $rs_ary['type_cd'] , PHPExcel_Cell_DataType::TYPE_STRING)
			->setCellValueExplicitByColumnAndRow($col++ , $row , $rs_ary['kamoku_cd'] , PHPExcel_Cell_DataType::TYPE_STRING)
			->setCellValueExplicitByColumnAndRow($col++ , $row , $rs_ary['image_cd'] , PHPExcel_Cell_DataType::TYPE_STRING)
			->setCellValueExplicitByColumnAndRow($col++ , $row , $rs_ary['layout_cd'] , PHPExcel_Cell_DataType::TYPE_STRING)
			->setCellValueByColumnAndRow($col++ , $row , $rs_ary['color_cd'])
			->setCellValueByColumnAndRow($col++ , $row , $rs_ary['open_flag'])
			->setCellValueByColumnAndRow($col++ , $row , $rs_ary['sort']!=""?$rs_ary['sort']:" ");
		$row++;
	} else {
		$col = 0;
		$csv_data_ary[$col++] = $rs_ary['id'];
		$csv_data_ary[$col++] = $rs_ary['site_name'];
		$csv_data_ary[$col++] = $rs_ary['site_url'];
		$csv_data_ary[$col++] = $rs_ary['keyword'];
		$csv_data_ary[$col++] = $rs_ary['type_cd'];
		$csv_data_ary[$col++] = $rs_ary['kamoku_cd'];
		$csv_data_ary[$col++] = $rs_ary['image_cd'];
		$csv_data_ary[$col++] = $rs_ary['layout_cd'];
		$csv_data_ary[$col++] = $rs_ary['color_cd'];
		$csv_data_ary[$col++] = $rs_ary['open_flag'];
		$csv_data_ary[$col++] = $rs_ary['sort'];
		mb_convert_variables('SJIS' , 'UTF8' , $csv_data_ary);
		fputcsv($stream, $csv_data_ary);
	}
}

//出力部分（EXCELなら書き込み）
if ($_GET['type'] == "xlsx") {
	$writer = PHPExcel_IOFactory::createWriter($book, 'Excel2007');
	$writer->save("./worklist.xlsx");
	$dl_file = "worklist.xlsx";
} elseif ($_GET['type'] == "xls") {
	$writer = PHPExcel_IOFactory::createWriter($book, 'Excel5');
	$writer->save("./worklist.xls");
	$dl_file = "worklist.xls";
} else {
	$dl_file = "worklist.csv";
}
//$dl_file = mb_convert_encoding($dl_file, "SJIS-win", "UTF-8");
// ヘッダ
header("Content-Type: application/octet-stream");
header("Content-Disposition: attachment; filename={$dl_file}");// ダイアログボックスに表示するファイル名
header("Content-Length: ".filesize($dl_file));
ob_start();
ob_end_clean();//出力バッファのゴミ掃除とオフ設定
// 対象ファイルを出力する。
readfile("./".$dl_file);
exit();
