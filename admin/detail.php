<?php
require_once("./common/config.inc");
$id = $_REQUEST['id'];
if(empty($id))	{
	die("不正なアクセスです");
}
$mode = $_REQUEST['mode'];

$db = DB_connect();

$error_flag	= 0;
if( $mode == 1 )	{//1:detail.phpのformでmode=1と定義
	$data	= $_REQUEST['data'];//detail.phpのformでdata配列を定義
	//エラーチェック

/*
	if( !checkdate($data['open_date']['m'],$data['open_date']['d'],$data['open_date']['Y']))	{
		$error['open_date']	= "公開日時が正しくありません";
		$error_flag++;
	}
	if( strlen(trim($data['category_id'])) == 0 )	{
		$error['category_id']	= "カテゴリが選択されてません";
		$error_flag++;
	}
	if( strlen(trim($data['title'])) == 0 )	{
		$error['title']	= "ページタイトルが未入力です";
		$error_flag++;
	}
	if( strlen(trim($data['file_name'])) > 0 )	{
		if (preg_match("/\A\w[\w+\-_ ]+$/", $data['file_name'])) {
		}else{
			$error['file_name']	= "使用できない文字が含まれています";
			$error_flag++;
		}
	}else{
		$error['file_name']	= "ファイル名が未入力です";
		$error_flag++;
	}
	if( strlen(trim($data['official_url'])) > 0 )	{
		if (preg_match("/^(https?|ftp)(:\/\/[-_.!~*\'()a-zA-Z0-9;\/?:\@&=+\$,%#]+)$/", $data['official_url'])) {
		}else{
			$error['official_url']	= "URLを正しく入力してください";
			$error_flag++;
		}
	}
	if( strlen(trim($data['report_url'])) > 0 )	{
		if (preg_match("/^(https?|ftp)(:\/\/[-_.!~*\'()a-zA-Z0-9;\/?:\@&=+\$,%#]+)$/", $data['report_url'])) {
		}else{
			$error['report_url']	= "URLを正しく入力してください";
			$error_flag++;
		}
	}
	if( strlen(trim($data['org_price'])) > 0 )	{
		if (preg_match("/^[0-9]+$/", $data['org_price'])) {
		}else{
			$error['org_price']	= "通常価格(税込)は数字で入力してください";
			$error_flag++;
		}
	}
	if( strlen(trim($data['price'])) > 0 )	{
		if (preg_match("/^[0-9]+$/", $data['price'])) {
		}else{
			$error['price']	= "販売価格(税込)は数字で入力してください";
			$error_flag++;
		}
	}
	if( strlen(trim($data['h1_text'])) == 0 )	{
		$error['h1_text']	= "キャッチコピーは必須項目です。";
		$error_flag++;
	}
	if( strlen(trim($data['read_text'])) == 0 )	{
		$error['read_text']	= "リード文は必須項目です。";
		$error_flag++;
	}
	if( strlen(trim($data['summary_title'])) == 0 )	{
		$error['summary_title']	= "概要タイトルは必須項目です。";
		$error_flag++;
	}
	if( strlen(trim($data['summary'])) == 0 )	{
		$error['summary']	= "概要は必須項目です。";
		$error_flag++;
	}
	if (preg_match("/^[0-9]+$/", $data['voting_num'])) {
	}else{
		$error['voting_num']	= "投稿人数は数字で入力してください";
		$error_flag++;
	}
	if (preg_match("/^([1-9]\d*|0)(\.\d+)?$/", $data['evaluation'])) {
	}else{
		$error['evaluation']	= "評価値を正しく入力してください";
		$error_flag++;
	}
*/
/*
画像UP処理
************************************************************/
//	var_dump($_FILES["upload_file"]);
	if ($_FILES["upload_file"]["tmp_name"] != "") { //画像送信しようとした場合
		if($_FILES["upload_file"]["type"] != "image/pjpeg" && $_FILES["upload_file"]["type"] != "image/jpeg")	{
		//JPEGファイルではない
			$error['img']	= "画像がJPEG形式ではありません";
			$error_flag++;
		} elseif($_FILES["upload_file"]["size"] > 2048*1024)	{
		//ファイルサイズが２MB以上
			$error['img']	= "ファイルサイズが多きすぎます２MB以下にしてください";
			$error_flag++;
		} elseif(!is_uploaded_file($_FILES["upload_file"]["tmp_name"]))	{
		//フォームからの送信でない
			$error['img']	= "画像がUPできませんでした";
			$error_flag++;
		} else {
			$tmp	= "../tmp/".$_FILES["upload_file"]["name"];
			$img	= "../images/site/".$id.".jpg";
			if(file_exists($img)) {
			//もしファイルが存在していたら削除
				unlink($img);
			}
			//アップロード
			move_uploaded_file($_FILES["upload_file"]["tmp_name"], $tmp);
			//画像サイズ取得
			$Jsize=getimagesize($tmp);
			if($Jsize[0] >= 201)	{
			//横121ピクセル以上なら
				$Jwidth		= 570; //横200ピクセル
				$Jheight	= $Jsize[1] * 570 / $Jsize[0]; //縦サイズを計算
				$imagein	= imagecreatefromjpeg($tmp); //画像を縮小する
				$imageout	= imagecreatetruecolor($Jwidth,$Jheight); //サイズ変更（GD2使用）
				imagecopyresampled($imageout,$imagein,0,0,0,0,$Jwidth,$Jheight,$Jsize[0],$Jsize[1]);
				imagejpeg($imageout,($img)); //サムネイル書き出し
				imagedestroy($imagein); //メモリを解放する
				imagedestroy($imageout); //メモリを解放する
			} else {
			//画像の横幅がもともと120ピクセル以下の場合は [thumb] ディレクトリにコピーします。
				copy($tmp,$img); //ファイルコピー
			}
			unlink($tmp); //[image] ディレクトリの画像を削除
			$img_name = $id.".jpg";
		}
//print "がぞう";
//exit();

	}
/*
画像UP処理
************************************************************/
	if( $error_flag < 1 )	{
		if (isset($_POST['del_flg']) && $_POST['del_flg'] == "on") {//削除フラグ
			mysql_query("delete from `site` where `id`=".$id);//データ削除
			if (file_exists("../images/site/".$id.".jpg")) {//画像有り
				unlink("../images/site/".$id.".jpg");//画像も削除
			}
			header("Location: ./index.php");
			exit();
		} else {
			$now = date('Y-m-d H:i:s');
			$upd_sql = "UPDATE site SET ";
			$upd_sql .= "site_name='".$data['site_name']."',";
			$upd_sql .= "site_url='".$data['site_url']."',";
			if( isset($data["keyword"]))	{
				$keyword = $data["keyword"];
				$upd_sql .= "keyword='".$keyword."',";
			}
			$upd_sql .= "type_cd=".$data['type_cd'].",";
			if( isset($data["kamoku_cd"]))	{
				$data["kamoku_cd"] = array_filter($data["kamoku_cd"], 'strlen');
				$kamoku_cd = implode(',',$data["kamoku_cd"]);
				$upd_sql .= "kamoku_cd='".$kamoku_cd."',";
			}
			if( isset($img_name))	{
				$upd_sql .= "img='".$img_name."',";
			}
			$upd_sql .= "image_cd=".$data['image_cd'].",";
			$upd_sql .= "layout_cd=".$data['layout_cd'].",";
			$upd_sql .= "color_cd='".$data['color_cd']."',";
			$upd_sql .= "memo='".$data['memo']."',";
			$upd_sql .= "open_flag=".$data['open_flag'].",";
			$upd_sql .= "sort=".$data["sort"].",";
			$upd_sql .= "c_user_id=".$admin['id'].",";
			$upd_sql .= "m_user_id=".$admin['id'].",";
			$upd_sql .= "modified='".$now."' WHERE id=".$id;
			mysql_query($upd_sql);

			header("Location: ./index.php?mode=completion");
			exit();
		}

	}else{
		$status = "<p class=\"err\">更新できませんでした</p>";
	}
}
//データを取得
if( !isset($data))	{
	$sql = "SELECT * FROM site WHERE id=".$id;
	$res = mysql_query($sql);
	$row = mysql_fetch_assoc($res);
	$data = $row;
}

$keyword = $data['keyword'];
//メタキーワード分割
$kamoku_data = explode(",",$data['kamoku_cd']);

//タグ分割
$tags = explode(",",$data['tag']);
mysql_close($db);
//var_dump($data);

?>
<?php
require_once("./include/header.inc");
?>
<div id="main">
<p class="pankuzu"><a href="./index.php">HOME</a>&nbsp;&raquo;&nbsp;｢<?php echo $data['site_name'];?>｣詳細</p>
<div class="contents">
<h2>アイテム管理</h2>
<h3>｢<?php echo $data['site_name'];?>｣詳細</h3>
<form id="login-form" name="login-form" method="post" action="" enctype="multipart/form-data">
<input type="hidden" name="mode" value="1" />
<input type="hidden" name="id" value="<?php echo $id; ?>" />
<?php echo (isset($status)) ? $status : ''; ?>
<table class="cp-blue border shadow rd">
<tr>
	<th width="180">公開フラグ</th>
	<td><select name="data[open_flag]">
		<option value="0"<?php echo ($data['open_flag'] == "0") ? ' selected' : ''; ?>>非公開</option>
		<option value="1"<?php echo ($data['open_flag'] == "1") ? ' selected' : ''; ?>>公開</option>
	</select></td>
</tr>
<tr>
	<th width="180">サイト名（クリニック名）</th>
	<td><input name="data[site_name]" type="text" size="105" value="<?php echo $data['site_name']; ?>">
	<?php echo (isset($error['site_name'])) ? '<br /><span class="red">'.$error['site_name'].'</span>' : ''; ?></td>
</tr>
<tr>
	<th width="180">URL</th>
	<td><input name="data[site_url]" type="text" size="105" value="<?php echo $data['site_url']; ?>">
	<?php echo (isset($error['site_url'])) ? '<br /><span class="red">'.$error['site_url'].'</span>' : ''; ?></td>
</tr>
<tr>
	<th>キーワード</th>
	<td><input name="data[keyword]" type="text" class="keyword" size="105" value="<?php echo $keyword; ?>">
	<?php echo (isset($error['keyword'])) ? '<br /><span class="red">'.$error['keyword'].'</span>' : ''; ?></td>
</tr>
<tr>
	<th width="180">タイプ</th>
	<td><?php
for( $i=0; $i<count($type_array); $i++ )	{
?>	
	<label class="clear"><input type="radio" name="data[type_cd]" value="<?php echo $type_array[$i]['code']; ?>"<?php echo ( $type_array[$i]['code'] == $data['type_cd'] ) ? ' checked' : ''; ?>> <?php echo $type_array[$i]['name']; ?></label>
<?php
}
?></td>
</tr>
<tr>
	<th>診療科目</th>
	<td><?php
for( $i=0; $i<count($kamoku_array); $i++ )	{
?>
	<label class="clear"><input type="checkbox" name="data[kamoku_cd][<?php echo $i; ?>]" value="<?php echo $kamoku_array[$i]['code']; ?>"<?php echo (in_array($kamoku_array[$i]['code'],$kamoku_data)) ? ' checked' : ''; ?>> <?php echo $kamoku_array[$i]['name']; ?></label>
<?php
}
?>
	<?php echo (isset($error['kamoku_cd'])) ? '<br /><span class="red">'.$error['kamoku_cd'].'</span>' : ''; ?></td>
</tr>
<tr>
	<th width="180">イメージ</th>
	<td><?php
for( $i=0; $i<count($image_array); $i++ )	{
?>	
	<label class="clear"><input type="radio" name="data[image_cd]" value="<?php echo $image_array[$i]['code']; ?>"<?php echo ( $image_array[$i]['code'] == $data['image_cd'] ) ? ' checked' : ''; ?>> <?php echo $image_array[$i]['name']; ?></label>
<?php
}
?></td>
</tr>
<tr>
	<th width="180">レイアウト</th>
	<td><?php
for( $i=0; $i<count($layout_array); $i++ )	{
?>	
	<label class="clear"><input type="radio" name="data[layout_cd]" value="<?php echo $layout_array[$i]['code']; ?>"<?php echo ( $layout_array[$i]['code'] == $data['layout_cd'] ) ? ' checked' : ''; ?>> <?php echo $layout_array[$i]['name']; ?></label>
<?php
}
?></td>
</tr>
<tr>
	<th width="180">カラー</th>
	<td><?php
for( $i=0; $i<count($color_array); $i++ )	{
?>	
	<label class="clear"><input type="radio" name="data[color_cd]" value="<?php echo $color_array[$i]['code']; ?>"<?php echo ( $color_array[$i]['code'] == $data['color_cd'] ) ? ' checked' : ''; ?>> <?php echo $color_array[$i]['name']; ?></label>
<?php
}
?></td>
</tr>
<tr>
	<th>画像</th>
	<td><?php if(file_exists("../images/site/".$id.".jpg")) {
	?>
	<p><img src="../images/site/<?php echo $id; ?>.jpg"></p>
	<?php
	}
	?>
	<input name="upload_file" type="file" class="file">
	<?php echo (isset($error['img'])) ? '<br /><span class="red">'.$error['img'].'</span>' : ''; ?></td>
</tr>
<tr>
	<th class="required">ソート</th>
	<td><input name="data[sort]" type="text" size="10" value="<?php echo $data['sort']; ?>">
	<?php echo (isset($error['sort'])) ? '<br /><span class="red">'.$error['sort'].'</span>' : ''; ?></td>
</tr>
</table>

<p class="btn"><label><input id="chk_del" type="checkbox" name="del_flg">削除</label><input type="submit" value="更新する" id="submit" /></p>
</form>
</div>
</div>
<script>
$(function(){
	$("#chk_del").on("click" , function(){
		if ($(this).prop("checked")) {
			alert("ここにチェックを入れたまま更新ボタンを押すと、一覧からこのサイトが削除されます");
		}
	});
});
</script>

<?php
require_once("./include/footer.inc");
?>