<?php
//タイプの種類
$type_array = array (
	array( 'code'=>'1','name'=>'クリニック・医院'),
	array( 'code'=>'2','name'=>'歯科'),
	array( 'code'=>'3','name'=>'接骨院'),
	array( 'code'=>'4','name'=>'どうぶつ病院'),
	array( 'code'=>'5','name'=>'サテライトサイト'),
	array( 'code'=>'6','name'=>'コーポレートサイト'),
	array( 'code'=>'7','name'=>'ECサイト'),
	array( 'code'=>'8','name'=>'スクール'),
	array( 'code'=>'9','name'=>'飲食店'),
	array( 'code'=>'9999','name'=>'その他'),
	);
foreach( $type_array as $t )	{
	$type_name[$t['code']] = $t['name'];
}
//科目の種類
$kamoku_array = array (
	array( 'code'=>'1','name'=>'総合診療科'),
	array( 'code'=>'2','name'=>'内科'),
	array( 'code'=>'3','name'=>'呼吸器科'),
	array( 'code'=>'4','name'=>'循環器科'),
	array( 'code'=>'5','name'=>'消火器科（胃腸科）'),
	array( 'code'=>'6','name'=>'外科'),
	array( 'code'=>'7','name'=>'整形外科'),
	array( 'code'=>'8','name'=>'形成外科'),
	array( 'code'=>'9','name'=>'美容外科'),
	array( 'code'=>'10','name'=>'脳神経外科'),
	array( 'code'=>'11','name'=>'小児科'),
	array( 'code'=>'12','name'=>'産科'),
	array( 'code'=>'13','name'=>'婦人科'),
	array( 'code'=>'14','name'=>'眼科'),
	array( 'code'=>'15','name'=>'放射線科'),
	array( 'code'=>'16','name'=>'肛門科'),
	array( 'code'=>'17','name'=>'皮膚科'),
	array( 'code'=>'18','name'=>'泌尿器科'),
	array( 'code'=>'19','name'=>'神経内科'),
	array( 'code'=>'20','name'=>'美容内科'),
	array( 'code'=>'21','name'=>'性病科'),
	array( 'code'=>'22','name'=>'心療内科'),
	array( 'code'=>'23','name'=>'精神科'),
	array( 'code'=>'24','name'=>'耳鼻咽喉科'),
	array( 'code'=>'25','name'=>'内分泌科'),
	array( 'code'=>'26','name'=>'リウマチ科'),
	array( 'code'=>'27','name'=>'麻酔科'),
	array( 'code'=>'28','name'=>'アレルギー科'),
	array( 'code'=>'29','name'=>'気管食道科'),
	array( 'code'=>'30','name'=>'リハビリステーション科'),
	array( 'code'=>'31','name'=>'人工透析'),
	array( 'code'=>'32','name'=>'糖尿病外来'),
	array( 'code'=>'33','name'=>'更年期外来'),
	array( 'code'=>'34','name'=>'乳腺外科'),
	array( 'code'=>'35','name'=>'癌検診'),
	array( 'code'=>'36','name'=>'人間ドック'),
	array( 'code'=>'37','name'=>'在宅医療'),
	array( 'code'=>'38','name'=>'訪問診療'),
	array( 'code'=>'39','name'=>'性機能障害'),
	array( 'code'=>'40','name'=>'ペインクリニック'),
	array( 'code'=>'41','name'=>'健康診断'),
	array( 'code'=>'42','name'=>'乳幼児健診'),
	array( 'code'=>'43','name'=>'予防接種'),
	array( 'code'=>'44','name'=>'生活習慣病'),
	array( 'code'=>'45','name'=>'甲状腺疾患'),
	array( 'code'=>'46','name'=>'禁煙外来'),
	array( 'code'=>'47','name'=>'自由診療'),
	array( 'code'=>'48','name'=>'美容皮膚科'),
	array( 'code'=>'49','name'=>'スポーツ整形外科'),
	array( 'code'=>'50','name'=>'骨粗しょう症'),
	array( 'code'=>'51','name'=>'ロコモーティブシンドローム'),
	array( 'code'=>'52','name'=>'巻き爪治療'),
	array( 'code'=>'53','name'=>'睡眠時無呼吸症候群'),
	array( 'code'=>'54','name'=>'花粉症'),
	array( 'code'=>'55','name'=>'交通事故診療'),
	array( 'code'=>'56','name'=>'労災治療'),
	array( 'code'=>'57','name'=>'AGA（男性脱毛症）'),
	array( 'code'=>'58','name'=>'小児整形外科'),
	array( 'code'=>'59','name'=>'装具外来'),
	array( 'code'=>'60','name'=>'モイストヒーリング（湿潤療法）'),
	array( 'code'=>'61','name'=>'関節外科'),
	array( 'code'=>'62','name'=>'脊椎・脊髄外科'),
	array( 'code'=>'63','name'=>'手術'),
	array( 'code'=>'64','name'=>'漢方内科'),
	array( 'code'=>'65','name'=>'思春期小児科'),
	array( 'code'=>'66','name'=>'肝臓内科'),
	array( 'code'=>'67','name'=>'内視鏡内科'),
	array( 'code'=>'68','name'=>'感染症外来'),
	array( 'code'=>'9999','name'=>'その他'),
	);
foreach( $kamoku_array as $k )	{
	$kamoku_name[$k['code']] = $k['name'];
}


//イメージの種類
$image_array = array (
	array( 'code'=>'1','name'=>'シンプル'),
	array( 'code'=>'2','name'=>'繊細'),
	array( 'code'=>'3','name'=>'賑やか'),
	array( 'code'=>'4','name'=>'清潔感・さわやか'),
	array( 'code'=>'5','name'=>'温かみのある雰囲気'),
	);
foreach( $image_array as $i )	{
	$image_name[$i['code']] = $i['name'];
}

//レイアウトの種類
$layout_array = array (
	array( 'code'=>'1','name'=>'2カラム（左）'),
	array( 'code'=>'2','name'=>'2カラム（右）'),
	array( 'code'=>'3','name'=>'3カラム'),
	array( 'code'=>'9999','name'=>'その他'),
	);
foreach( $layout_array as $l )	{
	$layout_name[$l['code']] = $l['name'];
}

//カラーの種類
$color_array = array (
	array( 'code'=>'white','name'=>'ホワイト系'),
	array( 'code'=>'blue','name'=>'ブルー系'),
	array( 'code'=>'green','name'=>'グリーン系'),
	array( 'code'=>'beige','name'=>'ベージュ系'),
	array( 'code'=>'black','name'=>'ブラック系'),
	array( 'code'=>'gray','name'=>'グレー系'),
	array( 'code'=>'brown','name'=>'ブラウン系'),
	array( 'code'=>'red','name'=>'レッド系'),
	array( 'code'=>'pink','name'=>'ピンク系'),
	array( 'code'=>'purple','name'=>'パープル系'),
	array( 'code'=>'yellow','name'=>'イエロー系'),
	array( 'code'=>'orange','name'=>'オレンジ系'),
	array( 'code'=>'colorful','name'=>'カラフル系'),
	);
foreach( $color_array as $c )	{
	$color_name[$c['code']] = $c['name'];
}
?>