/*------------------------------------------

オートページャー

------------------------------------------*/


jQuery(document).ready(function ($) {
	// 読み込むコンテンツの最大ページ数取得
	// ローディング画像を非表示
	$('#loading').css('display', 'none');
	var page = 2;
	var urlname=
	$.autopager({
		autoLoad: true,         // 自動読み込みするかどうか
		content	: '#contents #pic li',	// 自動読み込みしたいコンテンツ部分のセレクタ 

		link	: '#next a',	// 次ページへのリンクのセレクタ
		// 読み込みが開始された時に発生するイベント
		start: function(current, next){
			// ローディング画像を表示
			$('#loading').css('display', 'block');
			// 次ページのリンクを非表示
			$('#next a').css('display', 'none');
		},

		// 読み込み完了後に発生するイベント
		load: function(current, next){
			// ローディング画像を非表示
			$('#loading').css('display', 'none');
			// 次ページのリンクを表示
			$('#next a').css('display', 'block');
			// オートページャーアナリティクス解析
//			_gaq.push(['_trackPageview', location.pathname+'/page/'+ page +'/']);
			page++;
		}
});

});


