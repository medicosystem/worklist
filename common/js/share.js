
/*------------------------------------------

グローバルナビ高さ調整

------------------------------------------*/


    $(function () {
        // iPhone/iPad/iPod、Androidの場合は振り分けを判断
        if (navigator.userAgent.indexOf('iPhone') > 0 || navigator.userAgent.indexOf('iPad') > 0 || navigator.userAgent.indexOf('iPod') > 0 || navigator.userAgent.indexOf('Android') > 0) {


        } else {
            // PCの場合は以下を処理	
            $(function () {
                var h = $(window).height();
                var gNavi = $('#globalHeader');
                //スクロールが1に達したらボタン表示
                $(window).scroll(function () {
                    if ($(this).scrollTop() > 80) {
                        $("#globalHeader").stop().queue([]).animate({
							top:"-97px",
							paddingBottom:"13px"
                        }, 3000, "easeOutExpo");
 						$("#globalHeader h1 img").stop().queue([]).animate({
							width:"140px",
							height:"17px"
                        }, 3000, "easeOutExpo");
						$("#mainnavi ul li a.current").stop().queue([]).animate({
							paddingBottom:"13px"
                        }, 3000, "easeOutExpo");
 						$("#globalHeader h2 , #topicPath").queue([]).fadeOut(500, "easeOutExpo");						
						$("#globalHeader .boxSns").show();
						$("#pageTop img").queue([]).stop().animate({opacity:1 }, 2000, "easeOutExpo");
                    } else {
                        $("#globalHeader").stop().queue([]).animate({
                            top:0,
							paddingBottom:"54px"
                        }, 3000, "easeOutExpo");
 						$("#globalHeader h1 img").stop().queue([]).animate({
							width:"170px",
							height:"21px"
                        }, 3000, "easeOutExpo");
						$("#mainnavi ul li a.current").stop().queue([]).animate({
							paddingBottom:"54px"
                        }, 3000, "easeOutExpo");
 						$("#globalHeader h2 , #topicPath").show();
						$("#globalHeader .boxSns").queue([]).fadeOut(500, "easeOutExpo");
						$("#pageTop img").queue([]).stop().animate({opacity:0 }, 2000, "easeOutExpo");
                    }
                });
            });
        }
    });


/*------------------------------------------

スクロールカスタム

------------------------------------------*/


$(document).ready(
function() {
$(".box-lid-menu .navi").niceScroll({
             cursorcolor:"#d5d5d3", // スクロールバーの色
             cursorwidth:"5px", // スクロールバーの幅　
             cursoropacitymax:"1", // スクロールバーのアクティブ時の透明度
             cursoropacitymin:"1", // スクロールバーの非アクティブ時の透明度
             cursorborderradius:"3px", // スクロールバーの角丸
			 cursorborder:"none",
			 autohidemode: true,
            }); 
}
);

