<?php
/*********************************************************
全ページの
・｢title｣
・｢metaキーワード｣
・｢metaディスクリプション｣
・｢h1｣
・｢リード文｣
を設定
**********************************************************/

//デフォルト
$base_title		= "プラセンタ研究会 - 人気商品を口コミとランキングで徹底比較！";
$description	= 'プラセンタについて徹底的に調査しました。今人気のあるプラセンタは？口コミとランキングで徹底比較しました';
$keywords		= 'プラセンタ,口コミ,ランキング';
$h1				= 'プラセンタを徹底調査！今人気のプラセンタは？';
$lead			= '口コミとランキングで人気のプラセンタを徹底比較しました！あなたにあったプラセンタを見つけよう！';
if( $_SERVER['REQUEST_URI'] == '/about/' )	{
}else if( $_SERVER['REQUEST_URI'] == '/contact.html' ) {
 	$page_title			= "お問い合わせ | ".$site_name;
	$page_keywords		= $keywords.",お問い合わせ";
	$page_description	= $site_name."お問い合わせページです。当サイトへのご意見・ご要望などはお問い合わせフォームよりご連絡ください";
	$page_h1			= $site_name."お問い合わせページ";
	$page_lead			= "当サイトへのご意見・ご要望などはお問い合わせフォームよりご連絡ください";
}else if( $_SERVER['REQUEST_URI'] == '/howto.html' ) {
 	$page_title			= "投票・投稿の方法について | ".$site_name;
	$page_keywords		= $keywords.",投票,投稿,初心者";
	$page_description	= $site_name."の口コミランキングに対する評価の投票・口コミ投稿方法についてご説明いたします。";
	$page_h1			= "｢".$site_name."｣評価の投票・口コミ投稿方法について";
	$page_lead			= "口コミランキングに対する各商品への評価投票や口コミコメント投稿方法などをご説明";
}else if( $_SERVER['REQUEST_URI'] == '/privacy.html' ) {
 	$page_title			= "プライバシーポリシー | ".$site_name;
	$page_keywords		= $keywords.",投票,投稿,初心者";
	$page_description	= $site_name."のプライバシーポリシーについて。当サイトでは利用されるご訪問者様の個人情報を保護することに細心の注意を払っております。";
	$page_h1			= "｢".$site_name."｣の個人情報保護方針/プライバシーポリシー";
	$page_lead			= "当サイトではcookie等はサイト内広告の配信、及び訪問者の利用状況の確認のために利用しております。";
}else if( $_SERVER['REQUEST_URI'] == '/rank/' ) {
 	$page_title			= "プラセンタ人気総合口コミランキング｢今人気の商品はどれ？｣";
	$page_keywords		= $keywords.",人気,投稿,初心者";
	$page_description	= "ユーザーの投票で順位が変わるプラセンタ人気総合口コミランキング！今話題の人気の商品は？あなたにあったプラセンタをみつけてください！";
	$page_h1			= "今人気のプラセンタは？ユーザー投票で順位が決まる！";
	$page_lead			= "みんなの生声を商品選びの参考に！！今人気のプラセンタはコレだ！！";
	$page_exp			= "今、人気のプラセンタ総合TOP10をご紹介！<br />ユーザの投票で決まるランキング！商品選びの参考に！";
}else if( $_SERVER['REQUEST_URI'] == '/rank/suplee.html' ) {
 	$page_title			= "プラセンタサプリメント口コミランキング｢今人気の商品はどれ？｣";
	$page_keywords		= $keywords.",人気,投稿,初心者,サプリメント";
	$page_description	= "ユーザーの投票で順位が変わるプラセンタサプリメント口コミランキング！今話題の人気の商品は？BEST10を紹介します。";
	$page_h1			= "今人気のプラセンタサプリメントは？";
	$page_lead			= "みんなの生声を商品選びの参考に！！今人気のプラセンタサプリメントはコレだ！！";
	$page_exp			= "今、人気のプラセンタサプリメントTOP10をご紹介！<br />ユーザの投票で決まるプラセンタサプリメントランキング！";
}else if( $_SERVER['REQUEST_URI'] == '/rank/drink.html' ) {
 	$page_title			= "プラセンタドリンク口コミランキング｢今人気の商品はどれ？｣";
	$page_keywords		= $keywords.",人気,投稿,初心者,ドリンク";
	$page_description	= "ユーザーの投票で順位が変わるプラセンタドリンク口コミランキング！今話題の人気の商品は？BEST10を紹介します。";
	$page_h1			= "今人気のプラセンタドリンクは？";
	$page_lead			= "みんなの生声を商品選びの参考に！！今人気のプラセンタドリンクはコレだ！！";
	$page_exp			= "今、人気のプラセンタドリンクTOP10をご紹介！<br />ユーザの投票で決まるプラセンタドリンクランキング！";
}else if( $_SERVER['REQUEST_URI'] == '/rank/cosme.html' ) {
 	$page_title			= "プラセンタ化粧品口コミランキング｢今人気の商品はどれ？｣";
	$page_keywords		= $keywords.",人気,投稿,初心者,化粧品";
	$page_description	= "ユーザーの投票で順位が変わるプラセンタ化粧品口コミランキング！今話題の人気の商品は？BEST10を紹介します。";
	$page_h1			= "今人気のプラセンタ化粧品は？";
	$page_lead			= "みんなの生声を商品選びの参考に！！今人気のプラセンタ化粧品はコレだ！！";
	$page_exp			= "今、人気のプラセンタ化粧品TOP10をご紹介！<br />ユーザの投票で決まるプラセンタ化粧品ランキング！";
}else if( $_SERVER['REQUEST_URI'] == '/rank/jelly.html' ) {
 	$page_title			= "プラセンタゼリー口コミランキング｢今人気の商品はどれ？｣";
	$page_keywords		= $keywords.",人気,投稿,初心者,ゼリー";
	$page_description	= "ユーザーの投票で順位が変わるプラセンタゼリー口コミランキング！今話題の人気の商品は？BEST10を紹介します。";
	$page_h1			= "今人気のプラセンタゼリーは？";
	$page_lead			= "みんなの生声を商品選びの参考に！！今人気のプラセンタゼリーはコレだ！！";
	$page_exp			= "今、人気のプラセンタゼリーTOP10をご紹介！<br />ユーザの投票で決まるプラセンタ化粧品ランキング！";
}else if( $_SERVER['REQUEST_URI'] == '/item/' ) {
 	$page_title			= "プラセンタ研究会が厳選した商品を検索！プラセンタサーチ";
	$page_keywords		= $keywords.",人気,投稿,初心者,検索";
	$page_description	= "プラセンタ研究会が厳選した商品を一覧でご紹介します。あなたにあったプラセンタを見つけてください";
	$page_h1			= "プラセンタ研究会が厳選した商品を紹介";
	$page_lead			= "現在、プラセンタ研究会に登録されている全商品を一覧で紹介いたします";
	$page_exp			= "プラセンタ研究会が厳選した商品を全てご紹介いたします。";
}else if( $_SERVER['REQUEST_URI'] == '/item/suplee.html' ) {
 	$page_title			= "プラセンタ研究会が厳選したプラセンタサプリメントをご紹介";
	$page_keywords		= $keywords.",人気,投稿,初心者,検索,サプリメント";
	$page_description	= "プラセンタ研究会が厳選したプラセンタサプリメントを一覧でご紹介します。あなたにあったサプリメントを見つけてください";
	$page_h1			= "プラセンタ研究会が厳選したサプリを紹介";
	$page_lead			= "現在、プラセンタ研究会に登録されているプラセンタサプリを一覧で紹介いたします";
	$page_exp			= "プラセンタ研究会が厳選したプラセンタサプリメントを全てご紹介いたします。";
}else if( $_SERVER['REQUEST_URI'] == '/item/drink.html' ) {
 	$page_title			= "プラセンタ研究会が厳選したプラセンタドリンクをご紹介";
	$page_keywords		= $keywords.",人気,投稿,初心者,検索,ドリンク";
	$page_description	= "プラセンタ研究会が厳選したプラセンタドリンクを一覧でご紹介します。あなたにあったドリンクを見つけてください";
	$page_h1			= "プラセンタ研究会が厳選したドリンクを紹介";
	$page_lead			= "現在、プラセンタ研究会に登録されているプラセンタドリンクを一覧で紹介いたします";
	$page_exp			= "プラセンタ研究会が厳選したプラセンタドリンクを全てご紹介いたします。";
}else if( $_SERVER['REQUEST_URI'] == '/item/cosme.html' ) {
 	$page_title			= "プラセンタ研究会が厳選したプラセンタ化粧品をご紹介";
	$page_keywords		= $keywords.",人気,投稿,初心者,検索,化粧品";
	$page_description	= "プラセンタ研究会が厳選したプラセンタ化粧品を一覧でご紹介します。あなたにあった化粧品を見つけてください";
	$page_h1			= "プラセンタ研究会が厳選した化粧品を紹介";
	$page_lead			= "現在、プラセンタ研究会に登録されているプラセンタ化粧品を一覧で紹介いたします";
	$page_exp			= "プラセンタ研究会が厳選したプラセンタ化粧品を全てご紹介いたします。";
}else if( $_SERVER['REQUEST_URI'] == '/item/jelly.html' ) {
 	$page_title			= "プラセンタ研究会が厳選したプラセンタゼリーをご紹介";
	$page_keywords		= $keywords.",人気,投稿,初心者,検索,ゼリー";
	$page_description	= "プラセンタ研究会が厳選したプラセンタゼリーを一覧でご紹介します。あなたにあったゼリーを見つけてください";
	$page_h1			= "プラセンタ研究会が厳選したゼリーを紹介";
	$page_lead			= "現在、プラセンタ研究会に登録されているプラセンタゼリーを一覧で紹介いたします";
	$page_exp			= "プラセンタ研究会が厳選したプラセンタゼリーを全てご紹介いたします。";
}else if( $_SERVER['REQUEST_URI'] == '/about.html' ) {
 	$page_title			= "当サイトについて | ".$site_name;
	$page_keywords		= $keywords.",当サイトについて";
	$page_description	= "｢".$site_name."｣とはどんなサイトかを分かりやすく解説致します。";
	$page_h1			= "プラセンタ研究会とはどんなサイト？";
	$page_lead			= "当サイトはプラセンタに関する多く情報を掲載しています。";
}else if( $_SERVER['REQUEST_URI'] == '/link.html' ) {
 	$page_title			= "リンクについて | ".$site_name;
	$page_keywords		= $keywords.",リンクについて";
	$page_description	= "｢".$site_name."｣のリンクについて解説致します。";
	$page_h1			= "プラセンタ研究会では相互リンクを募集しています";
	$page_lead			= "相互リンクご希望のサイト運営者様はお気軽にお問合せフォームよりご連絡ください";
}else{
	$page_title			= $base_title;
	$page_keywords		= $keywords;
	$page_description	= $description;
	$page_h1			= $h1;
	$page_lead			= $lead;
}
?>