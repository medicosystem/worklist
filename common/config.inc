<?php
$site_name = "メディココンサルティング制作サイト集";
$site_url = "www.astgars.com";

require_once("./common/tag.inc");

//DB接続
function DB_connect()	{
	if ($_SERVER['REMOTE_ADDR'] == "127.0.0.1") {
		$db = mysql_connect("localhost","root","");
	} else {
		$db = mysql_connect("localhost","mccworklist","mcckakowork");
	}
	if(!$db)	{
		die("DBに接続できませんでした");
	}
	//DBの選択
	$conn = mysql_select_db("worklist",$db);
	if(!$conn)	{
		die("DBに接続できませんでした");
	}
	mysql_query("set names utf8");
	return $db;
}

/*
商品の製品情報取得
*******************************************************/
function GetIteminfoData($id)	{
	$db = DB_connect();
	$sql = "SELECT * FROM item_info WHERE item_id=".$id."";
	$res = mysql_query($sql);
	$row = mysql_fetch_assoc($res);
	return $row;
}

/*
商品の特徴取得
*******************************************************/
function GetItemfeatureData($id)	{
	$db = DB_connect();
	$sql = "SELECT * FROM item_feature WHERE item_id=".$id."";
	$res = mysql_query($sql);
	$row = mysql_fetch_assoc($res);
	return $row;
}

/*
商品情報取得
*******************************************************/
function GetItemData($file_name) {
	$db = DB_connect();
	$sql = "SELECT * FROM item WHERE file_name='".$file_name."'";
	$res = mysql_query($sql);
	$row = mysql_fetch_assoc($res);
	if(!$row)	{
		die("DBに接続できませんでした");
	}
	$item = $row;
	$eva_sql = "SELECT value FROM evaluation WHERE item_id=".$item['id'];
	$eva_res = mysql_query($eva_sql);
	$item['eva_num']=0;
	while( $eva_row = mysql_fetch_assoc($eva_res))	{
		$item['evaluation'] += $eva_row['value'];
		$item['eva_num'] += 1;
	}
	$item['ava'][0] = round($item['evaluation'] / ( $item['eva_num']+1),2);
	$item['ava'][1] = $item['voting_num'] + $item['eva_num'];

	return $item;
	mysql_close($db);
}

/*
商品タグ情報取得
*******************************************************/
function GetTagList($data) {
	if(!empty($data))	{
?>
<ul class="tag">
<?php
		$tags = explode(",",$data);
		foreach( $tags as $tag )	{
?>
	<li><?php echo $tag; ?></li>
<?php
		}
?>
</ul>
<?php
	}
}
/*
一覧用・商品タグ情報取得
*******************************************************/
function GetListTagData($data) {
	if(!empty($data))	{
		$taglist = explode(",",$data);
		$unset_array = array("定期購入有り","国産","トライアル","送料無料","返金保証");
		$tags = array_diff( $taglist, $unset_array);
?>
<aside><?php
		foreach( $tags as $tag )	{
?><span><?php echo $tag; ?></span><?php
		}
?></aside>
<?php
	}
}

/*
評価の投票履歴確認(同アイテムのIP重複チェック)
*******************************************************/
function ChkEvaluationData($id)	{
	$db = DB_connect();
	$sql = "SELECT * FROM evaluation WHERE item_id=".$id." AND ip_addr='".$_SERVER["REMOTE_ADDR"]."'";
	$res = mysql_query($sql);
	if(mysql_num_rows($res)) {
    	return "ng";
	}else{
    	return null;
	}
}

/*
投稿禁止IPチェック
*******************************************************/
function ChkNgIp()	{
	$db = DB_connect();
	$sql = "SELECT * FROM ng_ip WHERE ip_addr='".$_SERVER["REMOTE_ADDR"]."'";
	$res = mysql_query($sql);
	if(mysql_num_rows($res)) {
    	return "ng";
	}else{
    	return null;
	}
}

/*
口コミ投稿の投稿履歴確認(同アイテムのIP重複チェック及び一時間以内の投稿チェック)
*******************************************************/
function ChkReviewsData($id)	{
	$db = DB_connect();
	//一時間前
	$date = date("Y-m-d H:i:s",strtotime("-1 hour"));
	$sql = "SELECT * FROM reviews WHERE item_id=".$id." AND ip_addr='".$_SERVER["REMOTE_ADDR"]."' AND created >= '".$date."'";
	$res = mysql_query($sql);
	if(mysql_num_rows($res)) {
    	return "ng";
	}else{
    	return null;
	}
}

/*
口コミ投稿の件数取得
*******************************************************/
function GetTotalReviewsNum($id)	{
	$db = DB_connect();
	$sql = "SELECT count(id) FROM reviews WHERE item_id=".$id." AND open_flag=1";
	$res = mysql_query($sql);
	$row = mysql_fetch_row($res);
	$total = $row[0];
    return $total;
}

/*
口コミデータの取得
*******************************************************/
function GetReviewsData($id,$start,$limit)	{
	$db = DB_connect();
	$sql = "SELECT * FROM reviews WHERE item_id=".$id." AND open_flag=1 ORDER BY id DESC LIMIT ".$start.",".$limit;
	$res = mysql_query($sql);
	$i=0;
	while( $row = mysql_fetch_assoc($res))	{
		$data[$i] = $row;
		$i++;
	}
    return $data;
}

/*
ランキング一覧データ取得
*******************************************************/
function GetRankList($id=null) {
	$db = DB_connect();
	//ID順にデータを取得
	$now = date("Y-m-d H:i:s");
	if( !empty($id))	{
		$sql = "SELECT * FROM item WHERE open_flag=1 AND rank_flag=1 AND open_date <= '".$now."' AND category_id=".$id;
	}else{
		$sql = "SELECT * FROM item WHERE open_flag=1 AND rank_flag=1 AND open_date <= '".$now."'";
	}
	$res = mysql_query($sql);
	$i=0;
	while( $row = mysql_fetch_assoc($res))	{
		$rank[$i]	= $row;
		$eva_sql = "SELECT value FROM evaluation WHERE item_id=".$rank[$i]['id'];
		$eva_res = mysql_query($eva_sql);
		$rank[$i]['eva_num']=0;
		while( $eva_row = mysql_fetch_assoc($eva_res))	{
			$rank[$i]['evaluation'] += $eva_row['value'];
			$rank[$i]['eva_num'] += 1;
		}
		$rank[$i]['ava'][0] = round($rank[$i]['evaluation'] / ( $rank[$i]['eva_num']+1),2);
		$rank[$i]['ava'][1] = $rank[$i]['voting_num'] + $rank[$i]['eva_num'];
		$i++;
	}
	foreach( $rank as $e )	{
		$eva[] = $e['ava'][0];
	}
	array_multisort($eva, SORT_DESC,$rank);
	return $rank;
	mysql_close($db);
}

/*
登録アイテム一覧データ取得
*******************************************************/
function GetItemList($id=null) {
	$db = DB_connect();
	$now = date("Y-m-d H:i:s");
	if( !empty($id))	{
		$sql = "SELECT * FROM item WHERE open_flag=1 AND rank_flag=1 AND open_date <= '".$now."' AND category_id=".$id." ORDER BY modified DESC";
	}else{
		$sql = "SELECT * FROM item WHERE open_flag=1 AND rank_flag=1 AND open_date <= '".$now."' ORDER BY modified DESC";
	}
	$res = mysql_query($sql);
	$i=0;
	while( $row = mysql_fetch_assoc($res))	{
		$data[$i]	= $row;
		$i++;
	}
	return $data;
	mysql_close($db);
}

/*
カテゴリーデータ取得
*******************************************************/
function GetCateData($id) {
	$db = DB_connect();
	$now = date("Y-m-d H:i:s");
	$sql = "SELECT * FROM category WHERE open_flag=1 AND open_date <= '".$now."' AND id=".$id;
	$res = mysql_query($sql);
	$row = mysql_fetch_assoc($res);
	if(!$row)	{
		die("DBに接続できませんでした");
	}
	$cate = $row;
//	var_dump($row);
    return $cate;
	
}

/*
カテゴリー記事一覧データ取得
*******************************************************/
function GetCateList($id) {
	$db = DB_connect();
	$now = date("Y-m-d H:i:s");
	$sql = "SELECT * FROM page WHERE open_flag=1 AND open_date <= '".$now."' AND category_id=".$id." ORDER BY sort";
	$res = mysql_query($sql);
	$i=0;
	while( $row = mysql_fetch_assoc($res))	{
		$page[$i]	= $row;
		$i++;
	}
	return $page;
	mysql_close($db);
}

/*
記事バックナンバーリンク取得
*******************************************************/
function GetBackLink($id,$cate_id,$sort) {
	$db = DB_connect();
	$now = date("Y-m-d H:i:s");

	$back_sql = "SELECT * FROM page WHERE open_flag=1 AND open_date <= '".$now."' AND category_id=".$cate_id." AND id !=".$id." AND sort < ".$sort." ORDER BY sort DESC";
	$back_res = mysql_query($back_sql);
	$back_row = mysql_fetch_assoc($back_res);
	$backlink[0] = $back_row;

	$next_sql = "SELECT * FROM page WHERE open_flag=1 AND open_date <= '".$now."' AND category_id=".$cate_id." AND id !=".$id." AND sort > ".$sort." ORDER BY sort";
	$next_res = mysql_query($next_sql);
	$next_row = mysql_fetch_assoc($next_res);
	$backlink[1] = $next_row;
	
	return $backlink;
	mysql_close($db);
}

/*
トップページ用カテゴリー表示リストデータ取得
*******************************************************/
function GetPagecateList() {
	$db = DB_connect();
	$now = date("Y-m-d H:i:s");
	$sql = "SELECT * FROM category WHERE open_flag=1 AND open_date <= '".$now."' AND toplist_flag=1 ORDER BY sort";
	$res = mysql_query($sql);
	$i=0;
	while( $row = mysql_fetch_assoc($res))	{
		$pagecate[$i]	= $row;
		$i++;
	}
	return $pagecate;
	mysql_close($db);
}

/*
ページ情報取得
*******************************************************/
function GetPageData($file_name) {
	$db = DB_connect();
	$now = date("Y-m-d H:i:s");
	$sql = "SELECT * FROM page WHERE file_name='".$file_name."' AND open_flag=1 AND open_date <= '".$now."'";
	$res = mysql_query($sql);
	$row = mysql_fetch_assoc($res);
	if(!$row)	{
		die("DBに接続できませんでした");
	}
	$page = $row;
	return $page;
	mysql_close($db);
}

/*
ピックアップ記事取得
*******************************************************/
function GetPickUpData() {
	$db = DB_connect();
	$now = date("Y-m-d H:i:s");
	$sql = "SELECT * FROM page WHERE open_flag=1 AND open_date <= '".$now."' AND pickup_flag=1 ORDER BY modified DESC";
	$res = mysql_query($sql);
	$i=0;
	while( $row = mysql_fetch_assoc($res))	{
		$data[$i] = $row;
		$cate_sql = "SELECT file_name FROM category WHERE id=".$data[$i]['category_id'];
		$cate_res = mysql_query($cate_sql);
		$cate_row = mysql_fetch_assoc($cate_res);
		$data[$i]['cate_name'] = $cate_row['file_name'];
		$i++;
	}
//	var_dump($data);
    return $data;
	mysql_close($db);
}

/*
ページャー作成
*******************************************************/
function pager($c, $t) {
	$current_page = $c; 	//現在のページ
	$total_rec = $t;		//総レコード数
	$page_rec   = 10;	//１ページに表示するレコード
	$total_page = ceil($total_rec / $page_rec); //総ページ数
	$show_nav = 10;	//表示するナビゲーションの数
	$path = '?page=';	//パーマリンク
	
	//全てのページ数が表示するページ数より小さい場合、総ページを表示する数にする
	if ($total_page < $show_nav) {
		$show_nav = $total_page;
	}
	//トータルページ数が2以下か、現在のページが総ページより大きい場合表示しない
	if ($total_page <= 1 || $total_page < $current_page ) return;
	//総ページの半分
	$show_navh = floor($show_nav / 2);
	//現在のページをナビゲーションの中心にする
	$loop_start = $current_page - $show_navh;
	$loop_end = $current_page + $show_navh;
	//現在のページが両端だったら端にくるようにする
	if ($loop_start <= 0) {
		$loop_start  = 1;
		$loop_end = $show_nav;
	}
	if ($loop_end > $total_page) {
		$loop_start  = $total_page - $show_nav +1;
		$loop_end =  $total_page;
	}
	?>
	<p class="scott">
			<?php 
			//2ページ移行だったら「一番前へ」を表示
			if ( $current_page > 2)	{
				echo '<a href="'. $path .'1#comment"> &#171; </a>';
			}else{
				echo '<span class="disabled"> &#171; </span>';
			}
			//最初のページ以外だったら「前へ」を表示
			if ( $current_page > 1)	{
				echo '<a href="'. $path . ($current_page-1).'#comment"> &#139; </a>';
			}else{
				echo '<span class="disabled"> &#139; </span>';
			}
			for ($i=$loop_start; $i<=$loop_end; $i++) {
				if ($i > 0 && $total_page >= $i) {
					if($i == $current_page)	{
						echo '<span class="current">'.$i.'</span>';
					}else{
						echo '<a href="'. $path . $i.'#comment">'.$i.'</a>';
					}
				}
			}
			//最後のページ以外だったら「次へ」を表示
			if ( $current_page < $total_page)	{
				echo '<a href="'. $path . ($current_page+1).'#comment"> &#155; </a>';
			}else{
				echo '<span class="disabled"> &#155; </span>';
			}
			//最後から２ページ前だったら「一番最後へ」を表示
			if ( $current_page < $total_page - 1)	{
				echo '<a href="'. $path . $total_page.'#comment"> &#187; </a>';
			}else{
				echo '<span class="disabled"> &#187; </span>';
			}
			?>
	</p>
<?php
}
?>